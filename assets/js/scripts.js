jQuery(document).foundation();
jQuery(document).ready(function($) {

	// Wait for resize event to finish
	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();

	function galaxy_set_content_height() {
		$content_top = $content.offset().top;
		$footer_height = $('.footer').outerHeight();
		$content_min_height = $(window).outerHeight() - ( $content_top + $footer_height );

		// if (0 < $wpadminbar.length && 0 < $sticky_topbar.length) {
		// 	$content_min_height = ($content_min_height - $wpadminbar.outerHeight());
		// }

		$content.css('min-height', $content_min_height );
	}

	/* Make sure footer is always on the bottom by forcing the #content height */
	$content = $('#content');
	$wpadminbar = jQuery('#wpadminbar');
	$sticky_topbar = jQuery('.header .sticky');

	if (0 < $content.length) {
		galaxy_set_content_height();

		// Now, after the window is done resizing, set the contents min-height again
		$(window).resize(function () {
			waitForFinalEvent(function(){
				galaxy_set_content_height();
			}, 500, "min-content-height");
		});
	}
	if (0 < $wpadminbar.length && 0 < $sticky_topbar.length) {
		$sticky_topbar.addClass('adminsticky');
	}

	/* Smooth Scrolling */
	$('a[href*="#"]:not([href^="#panel"]):not([class*="label"])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {

			var target = $(this.hash);
			target = target.length ? target : $('[name=\"' + this.hash.slice(1) +'\"]');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
			return false;
			}
		}
	});

	/* Animate.css */
	function add_animation(selector, anim) {
		jQuery(selector).removeClass(anim).addClass(anim + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
			jQuery(this).removeClass(anim);
		});
	}

	function onSelectChange($select, $slider){
		$select.change(function(){
			$slider.slick('slickNext');
		});
	}

		// Slick Example on Kitchen Sink with all defaults
		if ($.fn.slick) {

			$slick_slider = $('#slider-selector-container .slider-preview');
			if ($slick_slider) {
				$slick_slider.slick({
					// accessibility: true, // type:boolean, default:true, Enables tabbing and arrow key navigation
					// adaptiveHeight: false, // type:boolean, default:false, Enables adaptive height for single slide horizontal carousels.
					autoplay: true, // type:boolean, default:false, Enables Autoplay
					autoplaySpeed: 10000, // type:int(ms), default:3000, Autoplay Speed in milliseconds
					arrows: true, // type:boolean, default:true, Prev/Next Arrows
					// asNavFor: null, // type:string, default:null, Set the slider to be the navigation of other slider (Class or ID Name)
					// appendArrows: $(element), // type:string, default:$(element), Change where the navigation arrows are attached (Selector, htmlString, Array, Element, jQuery object)
					// appendDots: $(element), // type:string, default:$(element), Change where the navigation dots are attached (Selector, htmlString, Array, Element, jQuery object)
					// prevArrow: $(''), // type:string (html|jQuery selector) | object (DOM node|jQuery object), default:&lt;button type="button" class="slick-prev"&gt;Previous&lt;/button&g&lt;button type="button" class="slick-prev"&gt;Previous&lt;/button&gt;t;, Allows you to select a node or customize the HTML for the "Previous" arrow.
					// nextArrow: $(''), // type:string (html|jQuery selector) | object (DOM node|jQuery object), default:&lt;button type="button" class="slick-next"&gt;Next&lt;/button&g&lt;button type="button" class="slick-next"&gt;Next&lt;/button&gt;t;, Allows you to select a node or customize the HTML for the "Next" arrow.
					// centerMode: false, // type:boolean, default:false, Enables centered view with partial prev/next slides. Use with odd numbered slidesToShow counts.
					// centerPadding: '50px', // type:string, default:'50px', Side padding when in center mode (px or %)
					// cssEase: 'ease', // type:string, default:'ease', CSS3 Animation Easing
					// customPaging: n/a, // type:function, default:n/a, Custom paging templates. See source for use example.
					dots: true, // type:boolean, default:false, Show dot indicators
					// dotsClass: 'slick-dots', // type:string, default:'slick-dots', Class for slide indicator dots container
					// draggable: true, // type:boolean, default:true, Enable mouse dragging
					// fade: false, // type:boolean, default:false, Enable fade
					// focusOnSelect: false, // type:boolean, default:false, Enable focus on selected element (click)
					easing: 'ease', // type:string, default:'linear', Add easing for jQuery animate. Use with easing libraries or default easing methods
					// edgeFriction: 0.15, // type:integer, default:0.15, Resistance when swiping edges of non-infinite carousels
					// infinite: true, // type:boolean, default:true, Infinite loop sliding
					// initialSlide: 0, // type:integer, default:0, Slide to start on
					// lazyLoad: 'ondemand', // type:string, default:'ondemand', Set lazy loading technique. Accepts 'ondemand' or 'progressive'
					// mobileFirst: false, // type:boolean, default:false, Responsive settings use mobile first calculation
					// pauseOnFocus: true, // type:boolean, default:true, Pause Autoplay On Focus
					// pauseOnHover: true, // type:boolean, default:true, Pause Autoplay On Hover
					// pauseOnDotsHover: false, // type:boolean, default:false, Pause Autoplay when a dot is hovered
					// respondTo: 'window', // type:string, default:'window', Width that responsive object responds to. Can be 'window', 'slider' or 'min' (the smaller of the two)
					// responsive: none, // type:object, default:none, Object containing breakpoints and settings objects (see demo). Enables settings sets at given screen width. Set settings to "unslick" instead of an object to disable slick at a given breakpoint.
					// responsive: [
					// 	{
					// 		breakpoint: 481, // medium up
					// 		settings: {
					// 			slidesToShow: 3,
					// 		}
					// 	},
					// 	{
					// 		breakpoint: 769, // large up
					// 		settings: {
					// 			slidesToShow: 4,
					// 		}
					// 	},
					// ],
					// rows: 1, // type:int, default:1, Setting this to more than 1 initializes grid mode. Use slidesPerRow to set how many slides should be in each row.
					// slide: '', // type:element, default:'', Element query to use as slide
					// slidesPerRow: 1, // type:int, default:1, With grid mode intialized via the rows option, this sets how many slides are in each grid row. dver
					// slidesToShow: 1, // type:int, default:1, # of slides to show
					// slidesToScroll: 1, // type:int, default:1, # of slides to scroll
					speed: 1000, // type:int(ms), default:300, Slide/Fade animation speed
					// swipe: true, // type:boolean, default:true, Enable swiping
					// swipeToSlide: false, // type:boolean, default:false, Allow users to drag or swipe directly to a slide irrespective of slidesToScroll
					// touchMove: true, // type:boolean, default:true, Enable slide motion with touch
					// touchThreshold: 5, // type:int, default:5, To advance slides, the user must swipe a length of (1/touchThreshold) * the width of the slider
					// useCSS: true, // type:boolean, default:true, Enable/Disable CSS Transitions
					// useTransform: true, // type:boolean, default:true, Enable/Disable CSS Transforms
					// variableWidth: false, // type:boolean, default:false, Variable width slides
					// vertical: true, // type:boolean, default:false, Vertical slide mode
					// verticalSwiping: false, // type:boolean, default:false, Vertical swipe mode
					// rtl: false, // type:boolean, default:false, Change the slider's direction to become right-to-left
					waitForAnimate: true, // type:boolean, default:true, Ignores requests to advance the slide while animating
					// zIndex: 1000, // type:number, default:1000, Set the zIndex values for slides, useful for IE9 and lower
				});
			}


				// Because something in slick is broken

			$select_in = $('.animation-selector-in');
			$select_out = $('.animation-selector-out');

			onSelectChange($select_in, $slick_slider);
			onSelectChange($select_out, $slick_slider);

			$slick_slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
				$select_value = $('.animation-selector-out').val();
				add_animation(jQuery('.slider-preview .slick-slide.slick-active'), $select_value);
				// $('.slider-preview .slick-slide').removeClass($select_value + ' animated');
				// $('.slider-preview [data-slick-index="' + nextSlide + '"]').addClass($select_value + ' animated');
			});
			$slick_slider.on('afterChange', function(event, slick, currentSlide){
				$select_value = $('.animation-selector-in').val();
				add_animation(jQuery('.slider-preview .slick-slide.slick-active'), $select_value);
				// $('.slider-preview .slick-slide').removeClass($select_value + ' animated');
				// $('.slider-preview [data-slick-index="' + currentSlide + '"]').addClass($select_value + ' animated');
			});
		} // end if slick exists

	jQuery('.modal-gallery-link').click(function(event){
		event.preventDefault();
		var idx = $(this).data('slide');
		var chosenSlide = '.slide' + idx;
		var chosenNav = '#slide-nav' + idx;

		$('#modal-gallery .orbit-slide').removeClass('is-active');
		$('#modal-gallery .orbit-slide').css('display', 'none');

		$(chosenSlide).css('display', 'list-item');
		$(chosenSlide).addClass('is-active');
		$(chosenSlide).addClass('is-in');
		$('.slide-nav').removeClass('is-active');
		$(chosenNav).addClass('is-active');
	});

	$testimonial_slider = $('#testimonials .slick-slider');
	if (0 < $testimonial_slider.length) {
		$testimonial_slider.slick({
			autoplay: true,
			autoplaySpeed: 5000,
			arrows: false,
			centerMode: true,
			centerPadding: '0px',
			cssEase: 'ease',
			dots: true,
			mobileFirst: true,
			responsive: [{
				breakpoint: 769,
				settings: {
					slidesToShow: 3,
				}
			}],
			slidesToShow: 1,
			swipeToSlide: true,
		});
	}

	if (typeof ScrollReveal === "function") {
		window.sr = ScrollReveal();

		// Customizing a reveal set
		sr.reveal('#px-hero .sr-in', {
			// 'bottom', 'left', 'top', 'right'
			origin: 'bottom',

			// Can be any valid CSS distance, e.g. '5rem', '10%', '20vw', etc.
			distance: '50%',

			// Time in milliseconds.
			duration: 1000,
			delay: 200,

			// Starting angles in degrees, will transition from these values to 0 in all axes.
			rotate: { x: 0, y: 0, z: 0 },

			// Starting opacity value, before transitioning to the computed opacity.
			opacity: 0,

			// Starting scale value, will transition from this value to 1
			scale: 0.5,

			// Accepts any valid CSS easing, e.g. 'ease', 'ease-in-out', 'linear', etc.
			easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',

			// `<html>` is the default reveal container. You can pass either:
			// DOM Node, e.g. document.querySelector('.fooContainer')
			// Selector, e.g. '.fooContainer'
			container: window.document.documentElement,

			// true/false to control reveal animations on mobile.
			mobile: true,

			// true:  reveals occur every time elements become visible
			// false: reveals occur once as elements become visible
			reset: true,

			// 'always' — delay for all reveal animations
			// 'once'   — delay only the first time reveals occur
			// 'onload' - delay only for animations triggered by first load
			useDelay: 'always',

			// Change when an element is considered in the viewport. The default value
			// of 0.20 means 20% of an element must be visible for its reveal to occur.
			viewFactor: 0.2,

			// Pixel values that alter the container boundaries.
			// e.g. Set `{ top: 48 }`, if you have a 48px tall fixed toolbar.
			// --
			// Visual Aid: https://scrollrevealjs.org/assets/viewoffset.png
			viewOffset: { top: 0, right: 0, bottom: 0, left: 0 },

			// Callbacks that fire for each triggered element reveal, and reset.
			beforeReveal: function (domEl) {},
			beforeReset: function (domEl) {},

			// Callbacks that fire for each completed element reveal, and reset.
			afterReveal: function (domEl) {},
			afterReset: function (domEl) {}
		}, 500);
	}

	// $('.px-hero').parallax({imageSrc: 'http://dsthedev.com/wp-content/uploads/2016/08/background-fancy-tiny.jpg'});

});

/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {

	// Remove empty P tags created by WP inside of Accordion and Orbit
	jQuery('.accordion p:empty, .orbit p:empty').remove();

	 // Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass( 'end' );

	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
		if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
			jQuery(this).wrap("<div class='widescreen flex-video'/>");
		} else {
			jQuery(this).wrap("<div class='flex-video'/>");
		}
	});

});

// Global constants
var __WPAS = {
    FORM : "#wp-advanced-search.wpas-ajax-enabled",
    CONTAINER : "#wpas-results",
    INNER : "#wpas-results-inner",
    DEBUG_CONTAINER : "#wpas-debug",
    PAGE_FIELD : "#wpas-paged",
    FORM_ID: "",
    KEY_PREFIX: "wpasInstance_",
    HASH: "",
    STORAGE_KEY: function() {
        return this.KEY_PREFIX + this.FORM_ID;
    }
};

jQuery(document).ready(function($) {

    __WPAS.FORM_ID = $('#wpas-id').val();
    __WPAS.HASH = $(__WPAS.FORM).data('ajax-url-hash');
    var CURRENT_PAGE = 1;

    /**
     *  Event listeners
     */

    $('form.wpas-autosubmit :input').change(function() {
        $(this).parents('form').submit();
    });

    $('button.wpas-clear').click(function(e) {
        e.preventDefault();
        $(this).parents('form').find(':input')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
        $(this).parents('form.wpas-autosubmit').each(function() {
            $(this).submit();
            return false;
        });
    });

    $('input.wpas-reset').click(function(e){
        e.preventDefault();
        $(this).parents('form')[0].reset();
        $(this).parents('form.wpas-autosubmit').each(function() {
            $(this).submit();
            return false;
        });
    });

    /**
     *  AJAX Functionality
     */

    if ($(__WPAS.FORM).length == 0) {
        log("No AJAX-enabled WPAS search form detected on page.");
        return;
    }

    if ($(__WPAS.CONTAINER).length == 0) {
        log("No container with ID #wpas-results found on page.  Results cannot be shown");
        return;
    }

    var DEBUG_ON = ($(__WPAS.FORM).hasClass('wpas-debug-enabled')) ? true : false;
    var SHOW_DEFAULT = ($(__WPAS.FORM).data('ajax-show-default')) ? true : false;

    var T = (DEBUG_ON) ? 500 : 0;

    if (DEBUG_ON && $(__WPAS.DEBUG_CONTAINER).length == 0) {
        log("WPAS_DEBUG is enabled but no container with ID #wpas-debug was found " +
        "on this page.  Debug information cannot be shown.");
        return;
    }

    var ajaxLoader = {
        container: "wpas-load",
        load_btn: "wpas-load-btn",
        load_btn_text: "",
        load_img: "wpas-loading-img",
        load_img_url: "",
        init : function(form) {
            this.load_btn_text = $(form).data('ajax-button');
            this.load_img_url = $(form).data('ajax-loading');
            $(__WPAS.CONTAINER).append(this.create());
        },

        create: function() {
            var html = "<div id='wpas-load'>";
            html += "<div><img id='"+this.load_img+"' style='display:none;' src='"+this.load_img_url+"'></div>";
            html += "<div><button id='"+this.load_btn+"' style='display:none;'>"+this.load_btn_text+"</button></div>";
            html += "</div>";
            return html;
        },

        showButton: function() {
            $('#'+this.load_btn).addClass('active').show();
        },

        hideButton: function() {
            $('#'+this.load_btn).removeClass('active').hide();
        },

        showImage: function() {
            $('#'+this.load_img).show();
        },

        hideImage: function() {
            $('#'+this.load_img).hide();
        }

    };

    $(__WPAS.CONTAINER).append("<div id='wpas-results-inner'></div>");
    ajaxLoader.init(__WPAS.FORM);

    var storage = null;
    if (window.location.hash.slice(1) == __WPAS.HASH) {
        storage = JSON.parse(localStorage.getItem("wpasInstance_"+__WPAS.FORM_ID));
    }

    if (storage != null) {
        log("localStorage found");
        loadInstance();
    } else {
        setPage(1);
        setRequest($(__WPAS.FORM).serialize());
    }

    if ($(__WPAS.CONTAINER).length != 0) {
        if (storage != null) {
            $(__WPAS.CONTAINER).html(storage.results);
        } else if (SHOW_DEFAULT) { // Show results by default if attribute is set
            sendRequest($(__WPAS.FORM).serialize(), CURRENT_PAGE);
        }
    }

    // Submits the form
    // Reset current page to 1
    function submitForm(form) {
        setPage(1);
        var form_data = $(form).serialize();
        setRequest(form_data);
        $(__WPAS.INNER).empty();
        sendRequest(form_data, CURRENT_PAGE);
    }

    // Set AJAX request to fetch results
    // Appends results to the container
    function sendRequest(data, page) {
        ajaxLoader.hideButton();
        ajaxLoader.showImage();
        jQuery.ajax({
            type: 'POST',
            url: WPAS_Ajax.ajaxurl,
            data: {
                action: 'wpas_ajax_load',
                page: page,
                form_data: data
            },
            success: function(data, textStatus, XMLHttpRequest) {
                response = JSON.parse(data);
                setTimeout(function() {
                    appendHTML(__WPAS.INNER, response.results);
                    ajaxLoader.hideImage();
                    updateHTML(__WPAS.DEBUG_CONTAINER,response.debug);
                    CURRENT_PAGE = response.current_page;
                    var max_page = response.max_page;

                    log("Current Page: "+CURRENT_PAGE+", Max Page: "+max_page);

                    if (max_page == 0 || CURRENT_PAGE == max_page) {
                        ajaxLoader.hideButton();
                    } else {
                        ajaxLoader.showButton();
                    }

                    window.location.hash = __WPAS.HASH;
                    storeInstance();
                    unlockForm();

                }, T);

            },
            error: function(MLHttpRequest, textStatus, errorThrown){
                console.log(errorThrown);
            }
        });
    }

    function storeInstance() {
        var instance = { request: REQUEST_DATA, form: getFormValues(), results : getResults(), page: CURRENT_PAGE  };
        instance = JSON.stringify(instance);
        localStorage.setItem(__WPAS.STORAGE_KEY(), instance);
    }

    function addArrayValues(values, input) {
        var name = $(input).attr('name');
        var value = $(input).val();

        if (typeof values[name] == 'undefined') {
            values[name] = [];
        }

        if ($(input).is(":checked")) values[name].push(value);

        return values;
    }

    function getFormValues() {
        var values = {};
        $(__WPAS.FORM).find(':input').not(':button, :submit, :reset').each(function() {
            if ($(this).attr('type') == 'checkbox') {
                values = addArrayValues(values, this)
            } else {
                values[$(this).attr('name')] = $(this).val();
            }
        });
        return values;
    }

    function getResults() {
        return $(__WPAS.CONTAINER).html();
    }

    function loadInstance() {
        var instance = localStorage.getItem(__WPAS.STORAGE_KEY());
        instance = JSON.parse(instance);
        if (instance == null) return;
        if (instance.form) loadForm(instance.form);
        if (instance.results) loadResults(instance.results);
        if (instance.page) setPage(instance.page);
        if (instance.request) setRequest(instance.request);
    }

    function loadForm(form_values) {
        $(__WPAS.FORM).find(':input').not(':button, :submit, :reset').each(function() {
            var value = form_values[$(this).attr('name')];
            if ($(this).attr('type') == 'checkbox') {
                if (value.indexOf( $(this).val() ) >= 0) {
                    $(this).prop('checked',true);
                } else {
                    $(this).prop('checked',false);
                }
            } else {
                $(this).val(value);
            }
        });
    }

    function loadResults(results) {
        $(__WPAS.CONTAINER).html(results);
    }

    function lockForm() {
        $(__WPAS.FORM).addClass('wpas-locked');
        $(__WPAS.FORM).find('input:submit').attr('disabled', 'disabled');
    }

    function formLocked() {
        return $(__WPAS.FORM).hasClass('wpas-locked');
    }

    function unlockForm() {
        $(__WPAS.FORM).removeClass('wpas-locked');
        $(__WPAS.FORM).find('input:submit').removeAttr('disabled');
    }

    function appendHTML(el, content) {
        $(el).append(content);
    }

    function updateHTML(el, content) {
        $(el).html(content);
    }

    function setPage(pagenum) {
        CURRENT_PAGE = pagenum;
        $(__WPAS.PAGE_FIELD).val(pagenum);
    }

    function setRequest(request) {
        REQUEST_DATA = request;
    }

    function log(msg) {
        if (DEBUG_ON) console.log("WPAS: " + msg);
    }

    // AJAX Event Listeners

    $(__WPAS.FORM).submit(function(e) {
        e.preventDefault();
        if (formLocked()) return;
        lockForm();
        submitForm(this);
    });

    $(document).on('click', '#'+ajaxLoader.load_btn+'.active', function(e){
        setPage(parseInt(CURRENT_PAGE) + 1)
        sendRequest(REQUEST_DATA,CURRENT_PAGE);
    });

});

(function ($) {
	$(".hamburger").on("click", function() {
	    // Toggle class "is-active"
	    $(this).toggleClass("is-active");
	    // Do something else, like open/close menu
		$('#navmenu').slideToggle();
		});
}(jQuery));



jQuery(document).ready(function($) {

	// Wait for resize event to finish
	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();

});
