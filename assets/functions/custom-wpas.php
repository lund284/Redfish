<?php /* Custom WPAS Functions

IMPORTANT: IGNORE THIS WHOLE SECTION, WPAS IS TRASH

*/

/**
Simple search box using WPAS

For more detailed/complex examples, look in galaxyTheme/assets/functions/wpas-config.php
*/
function galaxy_simple_wpas_search() {
	$args = array();

	// $args['debug'] = 'true';
	// $args['debug_level'] = 'verbose';

	// Settings
	$args['form'] = array(
		'auto_submit' => false, // Will trigger on radio/select changes, or pressing enter in input box
		'name'        => 'galaxy-wpas-simple-search', // Form name
		'action'      => home_url() . '/search-results/', // Where the form will lead (non-Ajax forms)
		'class'       => array('row', 'collapse', 'galaxy-simple-search'), // Array of classes to add
	);

	// Set default WP_Query
	$args['wp_query'] = array(
		'post_type' => array('post', 'page', /*'post_type'*/),
		'orderby' => 'title',
		'order' => 'ASC',
	);

	// Configure form fields
	$args['fields'][] = array(
		'type' => 'search',
		'placeholder' => 'WPASearch Simple...',
		// 'pre_html' => '<div class="large-8 small-9 columns">', // Wrap the entire field in this html
		// 'post_html' => '</div>', // End with this, should match tag from 'pre_html'
	);

	$args['fields'][] = array(
		'type' => 'submit',
		'class' => 'button small',
		'value' => 'Search', // Text on input/button, for icons, probably best to use 0 font size and CSS to position icon
		// 'pre_html' => '<div class="large-4 small-3 columns">',
		// 'post_html' => '</div>'
	);

	$args['fields'][] = array(
		'type' => 'html', // Used to inject bits of html, like <hr>/<br> etc.
		'value' => '<hr>',
	);

	register_wpas_form('galaxy-wpas-simple-search', $args); // Usage: new WP_Advanced_Search('galaxy-wpas-simple-search');
}
add_action('init', 'galaxy_simple_wpas_search');

/* How to use this query/form

** After get_header(), store original query to keep it in place:
global $wp_query; $original_query = $wp_query;
$galaxy_search = new WP_Advanced_Search('galaxy-search-default');
$wp_query = $galaxy_search->query();

# Loop through results, display them accordingly.

** At end of file, restore query, this is important in case there is any query based code in the footer!
$wp_query = $original_query;
wp_reset_query();

*/

/*EOF*/