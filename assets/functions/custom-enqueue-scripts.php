<?php // Register scripts and stylesheets
function galaxy_child_scripts_and_styles() {
	global $wp_styles;

	// Load What-Input files in footer
	wp_enqueue_script( 'what-input', get_template_directory_uri() . '/vendor/what-input/what-input.min.js', array(), '', true );

	// Adding Foundation scripts file in the footer
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/assets/js/foundation.js', array( 'jquery' ), '6.2', true );

	// Slick
	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/assets/js/slick/slick.min.js', array( 'jquery' ), '', true );
	wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/assets/js/slick/slick.min.css', array(), '', 'all' );
	wp_enqueue_style( 'slick-theme-css', get_template_directory_uri() . '/assets/js/slick/slick-theme.min.css', array(), '', 'all' );

	
	if ( is_page_template('templates/kitchen-sink.php') || is_page_template('templates/page-sandbox.php') ) {
			// scrollreveal
			wp_enqueue_script( 'scroll-reveal', get_template_directory_uri() . '/vendor/scrollreveal/dist/scrollreveal.min.js', array( 'jquery' ), '', true );
	}
	// Adding scripts file in the footer
	wp_enqueue_script( 'site-js', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );

	// Register main stylesheet
	wp_enqueue_style( 'site-css', get_stylesheet_directory_uri() . '/assets/css/style.css', array(), '', 'all' );
	wp_enqueue_style( 'default-css', get_stylesheet_directory_uri() . '/style.css', array(), '', 'all' );

	// Comment reply script for threaded comments
	if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
			wp_enqueue_script( 'comment-reply' );
	}
}
add_action('wp_enqueue_scripts', 'galaxy_child_scripts_and_styles', 999);

?>