// Grab our gulp packages
var gulp  = require('gulp'),
		gutil = require('gulp-util'),
		sass = require('gulp-sass'),
		cssnano = require('gulp-cssnano'),
		autoprefixer = require('gulp-autoprefixer'),
		sourcemaps = require('gulp-sourcemaps'),
		jshint = require('gulp-jshint'),
		stylish = require('jshint-stylish'),
		uglify = require('gulp-uglify'),
		concat = require('gulp-concat'),
		rename = require('gulp-rename'),
		plumber = require('gulp-plumber'),
		bower = require('gulp-bower'),
		babel = require('gulp-babel'),
		browserSync = require('browser-sync').create();

// Compile Sass, Autoprefix and minify
gulp.task('styles', function() {
	return gulp.src([
			'./assets/scss/**/*.scss'
			// ,'./assets/scss/animation/source/**/*.css'
			// ,'./assets/scss/custom/*.scss'
		])
		.pipe(plumber(function(error) {
			gutil.log(gutil.colors.red(error.message));
			this.emit('end');
		}))
		.pipe(sourcemaps.init()) // Start Sourcemaps
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('./assets/css/'))
		.pipe(rename({suffix: '.min'}))
		.pipe(cssnano())
		.pipe(sourcemaps.write('.')) // Creates sourcemaps for minified styles
		.pipe(gulp.dest('./assets/css/'))
});

/* SLICK!

// gulp.task('slickcss', function() {
// 	return gulp.src([
// 			'./assets/js/slick/*.scss'
// 			// ,'./assets/scss/custom/*.scss'
// 		])
// 		.pipe(plumber(function(error) {
// 			gutil.log(gutil.colors.red(error.message));
// 			this.emit('end');
// 		}))
// 		.pipe(sass())
// 		.pipe(autoprefixer({
// 			browsers: ['last 2 versions'],
// 			cascade: false
// 		}))
// 		.pipe(gulp.dest('./assets/js/slick/'))
// 		.pipe(rename({suffix: '.min'}))
// 		.pipe(cssnano())
// 		.pipe(gulp.dest('./assets/js/slick/'))
// });

// JSHint, concat, and minify JavaScript
gulp.task('scripts', function() {
	return gulp.src([
			// Grab your custom scripts
			'../galaxyTheme/assets/js/src/*.js',
  		'../galaxyTheme/wp-advanced-search/js/*.js',
			'./assets/js/src/*.js',
			// ,'./assets/js/src/custom.js'
		])
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(concat('scripts.js'))
		.pipe(gulp.dest('./assets/js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.pipe(sourcemaps.write('.')) // Creates sourcemap for minified JS
		.pipe(gulp.dest('./assets/js'))
});

// JSHint, concat, and minify Foundation JavaScript
gulp.task('foundationscripts', function() {
	return gulp.src([
			// Foundation core - required for using Foundation components
			'../galaxyTheme/vendor/foundation-sites/js/foundation.core.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.util.*.js',

			// Components
			'../galaxyTheme/vendor/foundation-sites/js/foundation.abide.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.accordion.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.accordionMenu.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.drilldown.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.dropdown.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.dropdownMenu.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.equalizer.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.interchange.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.magellan.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.offcanvas.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.orbit.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.responsiveMenu.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.responsiveToggle.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.reveal.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.slider.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.sticky.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.tabs.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.toggler.js',
			'../galaxyTheme/vendor/foundation-sites/js/foundation.tooltip.js',
		])
		.pipe(babel({
			presets: ['es2015'],
				compact: true
		}))
		.pipe(sourcemaps.init())
		.pipe(concat('foundation.js'))
		.pipe(gulp.dest('./assets/js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.pipe(sourcemaps.write('.')) // Creates sourcemap for minified Foundation JS
		.pipe(gulp.dest('./assets/js'))
});

// Update Foundation with Bower and save to /vendor
/* REMOVED, .BOWERRC FILE DOES THIS AUTOMATICALLY */
// gulp.task('bower', function() {
// 	return bower({ cmd: 'update'})
// 		.pipe(gulp.dest('vendor/'))
// });

// Browser-Sync watch files and inject changes
gulp.task('browsersync', function() {
	// Files to watch
	var files = [
		'./assets/css/*.css',
		'./assets/js/*.js',
		'./assets/js/slick/*.css',
		'**/*.php',
		'./assets/images/**/*.{png,jpg,gif,svg,webp}',
	];

	browserSync.init(files, {
		// Replace with your machines custom URL
		proxy: "http://192.168.0.187:8706/",
		// proxy: "http://192.168.0.134:1651/",
		// proxy: "http://192.168.0.150:284/",
		// proxy: "http://localhost/",
		port: "3030",
		open: false,
	});

	// gulp.watch('./assets/js/slick/*.scss', ['slickcss']);

	gulp.watch('./assets/scss/**/*.scss', ['styles']);
	gulp.watch('./assets/js/src/*.js', ['scripts']).on('change', browserSync.reload);
});

// Watch files for changes (without Browser-Sync)
gulp.task('watch', function() {
	gulp.watch('./assets/scss/**/*.scss', ['styles']);
	gulp.watch('./assets/js/src/*.js', ['scripts']);
	// gulp.watch('../galaxyTheme/vendor/foundation-sites/js/*.js', ['foundationscripts']);
});

// Run styles, scripts and foundationscripts
gulp.task('default', function() {
	gulp.start('browsersync');
	// gulp.start('styles', 'scripts', 'foundationscripts');
});
