<?php
	
$qcta = array(
	'qcta_content' => get_field('qcta_content'),
	'qcta_button_text' => get_field('qcta_button_text'),
	'qcta_button_link' => get_field('qcta_button_link')
);
/* 
	<?php echo $qcta['']; ?>
*/
?>

<div class="quick-cta-wrap">
	<div class="row">
		<div class="large-9 medium-8 small-8 columns">
			<h4><?php echo $qcta['qcta_content']; ?></h4>
		</div>
		<div class="large-3 medium-4 small-4 columns">
			<a href="<?php echo $qcta['qcta_button_link']; ?>" class="button red-button"><?php echo $qcta['qcta_button_text']; ?></a>
		</div>
	</div>
</div>

