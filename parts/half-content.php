<?php
	
$half_conImg = array(
	'hci_header_grey' => get_field('hci_header_grey'),
	'hci_header_red' => get_field('hci_header_red'),
	'hci_content' => get_field('hci_content'),
	'hci_button_text' => get_field('hci_button_text'),
	'hci_button_link' => get_field('hci_button_link'),
	'hci_image' => get_field('hci_image')
);
/* 
	'' => get_field(''),
	
	<?php echo $half_conImg['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>


<div class="half-content-wrap">
	<div class="row" data-equalizer="hc">
		<div class="large-6 medium-7 columns word-side" data-equalizer-watch="hc">
			<div class="wscv">
				<div class="redline"></div>
				<h3><?php echo $half_conImg['hci_header_grey']; ?><span><?php echo $half_conImg['hci_header_red']; ?></span></h3>
				<h5 class="alt"><?php echo $half_conImg['hci_content']; ?></h5>
				<a class="button tred-button" href="<?php echo $half_conImg['hci_button_link']; ?>"><?php echo $half_conImg['hci_button_text']; ?></a>
			</div>
		</div>
		<div class="large-6 medium-5 columns img-side" data-equalizer-watch="hc">
			<img src="<?php echo $half_conImg['hci_image']; ?>"/>
		</div>
	</div>
</div>

