<?php
	
$simpleContent = array(
	'sc_section_header' => get_field('sc_section_header'),
	'sc_content' => get_field('sc_content')
);
/* 
	'' => get_field(''),
	
	<?php echo $simpleContent['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<section class="simple-content-wrap">
	<div class="row">
		<h3><?php echo $simpleContent['sc_section_header']; ?></h3>
		<?php echo $simpleContent['sc_content']; ?>
	</div>
</section>

