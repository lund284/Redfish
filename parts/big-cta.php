<?php	
$bigCTA = array(
	'bcta_customize' => get_field('bcta_customize'),
	'bcta_header_red' => get_field('bcta_header_red'),
	'bcta_header_white' => get_field('bcta_header_white'),
	'bcta_content' => get_field('bcta_content'),
	'bcta_button_text' => get_field('bcta_button_text'),
	'bcta_button_link' => get_field('bcta_button_link'),
	'bcta_background_image' => get_field('bcta_background_image'),
);
$bigCTAto = array(
	'bcta_header_red' => get_field('bcta_header_red','options'),
	'bcta_header_white' => get_field('bcta_header_white','options'),
	'bcta_content' => get_field('bcta_content','options'),
	'bcta_button_text' => get_field('bcta_button_text','options'),
	'bcta_button_link' => get_field('bcta_button_link','options'),
	'bcta_background_image' => get_field('bcta_background_image','options'),
);
/* 
	'' => get_field(''),
	<?php echo $bigCTA['']; ?>
*/
?>
<?php if ($bigCTA['bcta_customize'] == 1){?>
<div class="big-cta-wrap" style="background-image: url(<?php echo $bigCTA['bcta_background_image']; ?>);">
	<div class="row">
		<h1><?php echo $bigCTA['bcta_header_red']; ?>&nbsp;<span><?php echo $bigCTA['bcta_header_white']; ?></span></h1>
		<h6 class="alt"><?php echo $bigCTA['bcta_content']; ?></h6>
		<a class="button twhite-button" href="<?php echo $bigCTA['bcta_button_link']; ?>"><?php echo $bigCTA['bcta_button_text']; ?></a>
	</div>
</div>
<?php } else { ?>
<div class="big-cta-wrap" style="background-image: url(<?php echo $bigCTAto['bcta_background_image']; ?>);">
	<div class="row">
		<h1><?php echo $bigCTAto['bcta_header_red']; ?>&nbsp;<span><?php echo $bigCTAto['bcta_header_white']; ?></span></h1>
		<h6 class="alt"><?php echo $bigCTAto['bcta_content']; ?></h6>
		<a class="button twhite-button" href="<?php echo $bigCTAto['bcta_button_link']; ?>"><?php echo $bigCTAto['bcta_button_text']; ?></a>
	</div>
</div>
<?php } ?>

