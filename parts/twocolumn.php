<?php
	
$flexContent = array(
	'fpc_section_header' => get_field('fpc_section_header'),
	'fpc_content' => get_field('fpc_content'),
	'' => get_field('')
);
/* 
	'' => get_field(''),
	
	<?php echo $flexContent['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<section class="flex-content-wrap">
	<div class="row">
		<div class="large-10 columns mc-side">
			<h3><?php echo $flexContent['fpc_section_header']; ?></h3>
			<?php echo $flexContent['fpc_content']; ?>
		</div>
		<div class="large-2 columns ql-side">
			quick links
		</div>
	</div>
</section>

