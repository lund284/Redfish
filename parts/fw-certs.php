<?php
	
$certs = array(
	'cert_content' => get_field('cert_content'),
	'cert_image_left' => get_field('cert_image_left'),
	'cert_image_right' => get_field('cert_image_right')
);
/* 
	'' => get_field(''),
	
	<?php echo $certs['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<div class="cert-wrap">
	<div class="row">
		<div class="large-6 medium-8 small-12 columns ws">
			<h4><?php echo $certs['cert_content']; ?></h4>
		</div>
		<div class="large-6 medium-4 small-12 columns is">
			<img src="<?php echo $certs['cert_image_left']; ?>"/>
			<img src="<?php echo $certs['cert_image_right']; ?>"/>
		</div>
	</div>
</div>

