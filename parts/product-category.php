<?php
	
$prod_cat = array(
	'pc_header_gray' => get_field('pc_header_gray'),
	'pc_header_red' => get_field('pc_header_red')
);
/* 	
	<?php echo $prod_cat['']; ?>
*/

?>

<div class="product-category-wrap">
	<div class="row">
		<?php if (!empty($prod_cat['pc_header_gray'])) {	?>		
		<div class="pc-header">
			<div class="redline"></div>
			<h3><?php echo $prod_cat['pc_header_gray']; ?>&nbsp;<span><?php echo $prod_cat['pc_header_red']; ?></span></h3>
		</div>
		<?php } ?>
		<div class="pc-contain" data-equalizer="pcat">
		<?php
		if( have_rows('pc_product_category') ):
		    while ( have_rows('pc_product_category') ) : the_row();
		    $prodImg = get_sub_field('image');
		?>    
			<div class="large-3 medium-6 columns p-cat">
				<div class="pc-inner">
				<?php if (!empty($prodImg)) { ?>
				<img src="<?php echo $prodImg; ?>"/>
				<?php } ?>
				<h5><?php the_sub_field('title');?></h5>
				<p data-equalizer-watch="pcat"><?php the_sub_field('tagline');?></p>
				<a href=" <?php the_sub_field('link_to');?>" class="pcat-link"><?php the_sub_field('link_text');?></a>
				</div>
			</div>
		       
		<?php     
		    endwhile;
		else :
		endif;
		?>
		</div>
	</div>
</div>

