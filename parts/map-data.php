<?php
	
/* 
	'' => get_field(''),
	
	<?php echo $['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<section class="acinfo-wrap">
	<div class="row">
	<?php
	if( have_rows('area_contact_info') ):
	    while ( have_rows('area_contact_info') ) : the_row();
	?>    
		<div class="large-4 columns acinfo">
			<h5><?php the_sub_field('title');?></h5>
			<p class="phone"><?php the_sub_field('phone');?></p>
			<p class="addr"><?php the_sub_field('address');?></p>
			<p class="email"><?php the_sub_field('email');?></p>
		</div>
	<?php     
	    endwhile;
	else :
	endif;
	?>
	</div>
</section>

