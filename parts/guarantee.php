<?php
	
$gurantee = array(
	'gs_icon_l' => get_field('gs_icon_l'),
	'gs_title_l' => get_field('gs_title_l'),
	'gs_content_l' => get_field('gs_content_l'),

	'gs_icon_m' => get_field('gs_icon_m'),
	'gs_title_m' => get_field('gs_title_m'),
	'gs_content_m' => get_field('gs_content_m'),

	'gs_icon_r' => get_field('gs_icon_r'),
	'gs_title_r' => get_field('gs_title_r'),
	'gs_content_r' => get_field('gs_content_r'),
);
/* 
	'' => get_field(''),
	
	<?php echo $gurantee['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<section class="guarantee-wrap">
	<div class="row" data-equalizer="gblock" equalize-on-stack="false">
		<div class="medium-4 columns g-block" data-equalizer-watch="gblock">
			<img src="<?php echo $gurantee['gs_icon_l']; ?>" class="gblock-img">
			<h5><?php echo $gurantee['gs_title_l']; ?></h5>
			<p><?php echo $gurantee['gs_content_l']; ?></p>
		</div>
		<div class="medium-4 columns g-block" data-equalizer-watch="gblock">
			<img src="<?php echo $gurantee['gs_icon_m']; ?>" class="gblock-img">
			<h5><?php echo $gurantee['gs_title_m']; ?></h5>
			<p><?php echo $gurantee['gs_content_m']; ?></p>
		</div>
		<div class="medium-4 columns g-block" data-equalizer-watch="gblock">
			<img src="<?php echo $gurantee['gs_icon_r']; ?>" class="gblock-img">
			<h5><?php echo $gurantee['gs_title_r']; ?></h5>
			<p><?php echo $gurantee['gs_content_r']; ?></p>
		</div>
	</div>
</section>

