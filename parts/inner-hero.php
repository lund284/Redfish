<?php
	
$innerHero = array(
	'ih_background_image' => get_field('ih_background_image'),
	'ih_gray_header' => get_field('ih_gray_header'),
	'ih_red_header' => get_field('ih_red_header')
);
/* 
	'' => get_field(''),
	
	<?php echo $innerHero['']; ?>

*/
?>

<section class="inner-hero-wrap" style="background-image: url(<?php echo $innerHero['ih_background_image']; ?>)">
	<div class="row">
		<div class="ihredline"></div>
		<h1><?php echo $innerHero['ih_gray_header']; ?>&nbsp;<span><?php echo $innerHero['ih_red_header']; ?></span></h1>
	</div>
</section>

