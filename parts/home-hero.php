<?php
$home_hero = array(
	'hh_background_image' => get_field('hh_background_image'),
	'hh_white_header' => get_field('hh_white_header'),
	'hh_red_header' => get_field('hh_red_header'),
	'hh_sub_header' => get_field('hh_sub_header'),
	'hh_white_button_text' => get_field('hh_white_button_text'),
	'hh_red_button_text' => get_field('hh_red_button_text'),
	'hh_white_button_link' => get_field('hh_white_button_link'),
	'hh_red_button_link' => get_field('hh_red_button_link')
);
/* 	
	<?php echo $home_hero['']; ?>
*/
?>

<div class="home-hero-wrap" style="background-image: url(<?php echo $home_hero['hh_background_image']; ?>);">
	<div class="row">
		<div class="large-8 medium-10 small-12 columns end hh-content">
			<h1><?php echo $home_hero['hh_white_header']; ?>&nbsp;<span><?php echo $home_hero['hh_red_header']; ?></span></h1>
			<h5><?php echo $home_hero['hh_sub_header']; ?></h5>
			<div class="buttons">
				<a href="<?php echo $home_hero['hh_white_button_link']; ?>" class="button twhite-button"><?php echo $home_hero['hh_white_button_text']; ?></a>
				<a href="<?php echo $home_hero['hh_red_button_link']; ?>" class="button tred-button"><?php echo $home_hero['hh_red_button_text']; ?></a>
			</div>
		</div>
	</div>
</div>