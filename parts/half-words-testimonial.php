<?php
	
$h_words_test = array(
	'hwht_header_left' => get_field('hwht_header_left'),
	'hwht_content_left' => get_field('hwht_content_left'),
	'hwht_testimonial' => get_field('hwht_testimonial'),
	'hwht_author' => get_field('hwht_author'),
	'hwht_background_image' => get_field('hwht_background_image')
);
/* 
	'' => get_field(''),
	
	<?php echo $h_words_test['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>

<section class="half-wt-wrap" style="background-image: url(<?php echo $h_words_test['hwht_background_image']; ?>);">
	<div class="row">
		<div class="large-6 columns plain">
			<div class="plain-inner">
				<div class="whiteline"></div>
				<h3><?php echo $h_words_test['hwht_header_left']; ?></h3>
				<h6 class="alt"><?php echo $h_words_test['hwht_content_left']; ?></h6>
			</div>
		</div>
		<div class="large-6 columns testimonial">
			<div class="testimonial-inner">
				<h5><?php echo $h_words_test['hwht_testimonial']; ?></h5>
				<h6 class="alt"><?php echo $h_words_test['hwht_author']; ?></h6>
			</div>
		</div>
	</div>
</section>

