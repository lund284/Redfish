<?php
	
$medCTA = array(
	'mcta_red_text' => get_field('mcta_red_text'),
	'mcta_gray_text' => get_field('mcta_gray_text'),
	'mcta_button_text' => get_field('mcta_button_text'),
	'mcta_button_link' => get_field('mcta_button_link')
);
/* 
	'' => get_field(''),	
	<?php echo $medCTA['']; ?>
*/
?>

<section class="mcta-wrap">
	<div class="row">
		<div class="large-8 columns words">
			<h5><?php echo $medCTA['mcta_red_text']; ?>&nbsp;<span><?php echo $medCTA['mcta_gray_text']; ?></span></h5>
		</div>
		<div class="large-4 columns link">
			<a href="<?php echo $medCTA['mcta_button_link']; ?>" class="button red-button"><?php echo $medCTA['mcta_button_text']; ?></a>
		</div>
	</div>
</section>

