<?php
$footer_opt = array(
	'fo_copyright' => get_field('fo_copyright', 'options'),
	'fo_phone' => get_field('fo_phone', 'options')
);
/* 
	'' => get_field(''),
	
	<?php echo $footer_opt['']; ?>
	
	<?php
	if( have_rows('repeater_field_name') ):
	    while ( have_rows('repeater_field_name') ) : the_row();
	?>    
	        <?php the_sub_field('sub_field_name');?>
	<?php     
	    endwhile;
	else :
	endif;
	?>
*/
?>
					<footer class="footer" role="contentinfo" id="stickyBtmAnchor"><?php // stickyBtmAnchor is required for the sticky top bar to work right ?>
						<div class="row" data-equalizer="footer" data-equalize-on-stack="false">
							<div class="large-3 medium-6 columns copyright" data-equalizer-watch="footer">
								<p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. <?php echo $footer_opt['fo_copyright']; ?></p>
							</div>
							<div class="large-3 medium-6 columns social" data-equalizer-watch="footer">
								<?php
								if( have_rows('fo_social_icons', 'options') ):
								    while ( have_rows('fo_social_icons', 'options') ) : the_row();
								?>    
								        <a href="<?php the_sub_field('link');?>"><img class="social-ftr" src="<?php the_sub_field('icon');?>" /></a>
								<?php     
								    endwhile;
								else :
								endif;
								?>
							</div>
							<div class="large-2 medium-6 columns phone" data-equalizer-watch="footer">
								<?php echo $footer_opt['fo_phone']; ?>
							</div>
							<div class="large-4 medium-6 columns images" data-equalizer-watch="footer">
								<?php
								if( have_rows('fo_images', 'options') ):
								    while ( have_rows('fo_images', 'options') ) : the_row();
								?>    
								        <img class="footer-img" src="<?php the_sub_field('image');?>" />
								<?php     
								    endwhile;
								else :
								endif;
								?>								
							</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->