<?php /* Template Name: What We Do Page */

get_header(); ?>

	<div id="content" class="wwd-page">
		<?php
			get_template_part( 'parts/inner', 'hero' );
			get_template_part( 'parts/guarantee');
			get_template_part('parts/half','words-testimonial');
			get_template_part( 'parts/big', 'cta' );
		?>
				
	</div> <!-- end #content -->

<?php get_footer(); ?>