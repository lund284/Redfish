<?php /* Template Name: 2 Column Page */

get_header(); ?>

	<div id="content" class="two-column-page">
		<?php
			get_template_part( 'parts/inner-hero' );
			get_template_part('parts/twocolumn');
			get_template_part( 'parts/big-cta' );
		?>
				
	</div> <!-- end #content -->

<?php get_footer(); ?>