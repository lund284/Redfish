<?php /* Template Name: Custom Template */

get_header(); ?>

	<div id="content">

		<div id="inner-content" class="row">

			<main id="main" class="column medium-4 large-6" role="main">

				<?php galaxy_breadcrumbs();

				if (have_posts()) {

					while (have_posts()) {

						the_post(); // initialize $post variable and the_ functions like the_title()

						get_template_part( 'parts/loop', 'page' );

					}

				} ?>

			</main> <!-- end #main -->

			<?php get_sidebar(); ?>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>