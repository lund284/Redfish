<?php /* Template Name: Product Page */

get_header(); ?>

	<div id="content" class="products-page">
		<?php
			get_template_part( 'parts/inner', 'hero' );
			get_template_part( 'parts/product', 'category' );
			get_template_part('parts/medium','cta');
			get_template_part( 'parts/big', 'cta' );
		?>
				
	</div> <!-- end #content -->

<?php get_footer(); ?>