<?php /* Template Name: Contact Page */

get_header(); ?>

	<div id="content" class="contact-page">
		<?php
			get_template_part( 'parts/inner-hero' );
			get_template_part( 'parts/simple-content');
			get_template_part('parts/contact-form');
			//get_template_part( 'parts/' );
		?>
				
	</div> <!-- end #content -->

<?php get_footer(); ?>