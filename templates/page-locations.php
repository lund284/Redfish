<?php /* Template Name: Territories Page */

get_header(); ?>

	<div id="content" class="territories-page">
		<?php
			get_template_part( 'parts/inner', 'hero' );
			get_template_part( 'parts/simple', 'content');
			get_template_part('parts/map');
			get_template_part( 'parts/map-data' );
		?>
				
	</div> <!-- end #content -->

<?php get_footer(); ?>