<?php /* Template Name: Home Page */

get_header(); ?>

	<div id="content" class="home-page">
		<?php
			get_template_part( 'parts/home', 'hero' );
			get_template_part( 'parts/quick', 'cta' );
			get_template_part( 'parts/product', 'category' );
			get_template_part( 'parts/fw', 'certs' );
			get_template_part( 'parts/half', 'content' );
			get_template_part( 'parts/big', 'cta' );
		?>
				
	</div> <!-- end #content -->

<?php get_footer(); ?>